# Admin Dashboard UI with Ant Design in React

Welcome to the Admin Dashboard UI built with Ant Design in React! This dashboard provides a user-friendly interface to manage and monitor your application.

### Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/admin-dashboard.git
   ```

2. Change into the project directory:

```bash

cd admin-dashboard
```
3. Install dependencies:

```bash

npm install
```
3. Usage
Start the development server:

```bash
npm start
```
Open your browser and navigate to http://localhost:3000 to view the dashboard.

Folder Structure
<ul>
    <li>src: Contains the source code of the React application.</li>
        <ul>
            <li>assets : Images used.</li>
            <li>components: Reusable React components.</li>
        </ul>
    <li> index.css: CSS or SCSS files for styling.</li>
      
Acknowledgments
  -Ant Design: A fantastic design system for React applications.
