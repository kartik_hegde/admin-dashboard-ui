import React from "react";
import "../index.css";
import { Layout, Space, Image } from "antd";
import graph from "../assets/graph.png";
import Title from "antd/es/skeleton/Title";

const { Content, Footer } = Layout;

const Contents = (props) => {
  const { collapsed } = props;
  return (
    <div>
      <Content
        style={{
          marginLeft: "10vw",
          overflow: "initial",
        }}
      >
        {!collapsed && (
          <div
            className="overlay"
            style={{
              position: "fixed",
              top: 0,
              left: 200,
              right: 0,
              bottom: 0,
              backgroundColor: "rgba(0, 0, 0, 0.5)",
              zIndex: 1,
            }}
          ></div>
        )}
        <div>
          <Space className="boxes">
          
          
            {/* cards section */}
            <Space className="box">
              <Space class="stats shadow">
                <Space class="stat">
                  <Space class="stat-title">Total User</Space>
                  <Space class="stat-value">89,400</Space>
                  <Space class="stat-desc">21% more than last month</Space>
                </Space>
              </Space>
            </Space>
            <Space className="box">
              <Space class="stats shadow">
                <Space class="stat">
                  <Space class="stat-title">Total Order</Space>
                  <Space class="stat-value">13,400</Space>
                  <Space class="stat-desc">26% more than last month</Space>
                </Space>
              </Space>
            </Space>
            <Space className="box">
              <Space class="stats shadow">
                <Space class="stat">
                  <Space class="stat-title">Total Sales</Space>
                  <Space class="stat-value">20,400</Space>
                  <Space class="stat-desc">11% more than last month</Space>
                </Space>
              </Space>
            </Space>
            <Space className="box">
              <Space class="stats shadow">
                <Space class="stat">
                  <Space class="stat-title">Total Pending</Space>
                  <Space class="stat-value">400</Space>
                  <Space class="stat-desc">2% more than last month</Space>
                </Space>
              </Space>
            </Space>
          </Space>


     {/* graph section */}
          <Space className="graph">
            <Space>
              {" "}
              <h2>Sales Details</h2>{" "}
            </Space>
            <Space>
              {" "}
              <Image className="image" src={graph} preview={false}></Image>
            </Space>
          </Space>

          <Space className="table"></Space>
        </div>
      </Content>
      
      {/* footer section */}
      <Footer
        style={{
          marginLeft: "10vw",
          textAlign: "center",
        }}
      >
        Ant Design ©{new Date().getFullYear()} Created by Hegde
      </Footer>
    </div>
  );
};

export default Contents;
