import React, { useState } from "react";
import { BellOutlined, UserOutlined } from "@ant-design/icons";
import {
  Layout,
  Input,
  Badge,
  Typography,
  Space,
  Image,
  Select,
  Button,
} from "antd";
import "../index.css";
import flag1 from "../assets/flag.png";
import flag2 from "../assets/usflag.jpg";
const { Title } = Typography;
const { Header } = Layout;
const { Search } = Input;

const Headers = () => {
  const [flag, setflag] = useState(flag1);

  const changeflag = (value) => {
    if (value === "hindi") {
      setflag(flag1);
    } else {
      setflag(flag2);
    }
  };
  const onSearch = () => {};
  return (
    <div>
      {" "}
      <Header
        className="header"
        style={{
          padding: 0,
          backgroundColor: "white",
          marginLeft: 5,
        }}
      >
        <Space>
          <Space className="header-content">
            {" "}
            <Space>
              {" "}
             {/* title block */}
             
              <Title
                className="title"
                style={{
                  marginLeft: "10vw",
                  marginTop: "2vh",
                  fontSize: "40px ",
                }}
              >
                <Space style={{ color: "#24a0ed" }}>DASH</Space> BOARD
              </Title>
            </Space>

            {/* searchbar */}
            <Space className="header-item">
              {" "}
              <Search
                className="searchbar"
                placeholder="input search text"
                allowClear
                onSearch={onSearch}
              />
            </Space>

             {/* notification with badge */}
            <Space style={{ padding: "2vh" }}>
              <Button className="button">
                {" "}
                <Badge count={5}>
                  <BellOutlined className="icon" />
                </Badge>
              </Button>{" "}
              <Image className="flag" preview={false} src={flag}></Image>
              <Select
                style={{ border: "none", paddingBottom: "8px" }}
                defaultValue="Hindi"
                onChange={changeflag}
                options={[
                  {
                    value: "english",
                    label: "English",
                  },
                  {
                    value: "hindi",
                    label: " Hindi  ",
                  },
                ]}
              ></Select>
            </Space>
            {/* user block */}
            <Space>
              {" "}
              <Button className="button">
                <UserOutlined className="icon" />
                Hegde ....
              </Button>
            </Space>
          </Space>{" "}
        </Space>{" "}
      </Header>
    </div>
  );
};

export default Headers;
