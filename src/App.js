
import Sidebar from './components/sidebar';
import Headers from './components/Header';
import Contents from './components/Contents';
import React ,{useState} from 'react';

import { Layout, } from 'antd';

function App() {
  const [collapsed, setCollapsed] = useState(true);

  return (
    <>
    <Layout>
    <Sidebar collapsed={collapsed} setCollapsed={setCollapsed}></Sidebar>
    <Layout>
      <Headers ></Headers>
      <Contents collapsed={collapsed} ></Contents>
    </Layout>
    </Layout>
    </>
  );
}

export default App;
